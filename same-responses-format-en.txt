year,2020
semester,2
classname, EC1

Registered,98
Responded,37

5,Very Much Agree
4,Agree Somehwat
3,Can't Say
2,Disagree Somewhat
1,Completely Disagree

7,The teacher's voice was audible during class.
7,14,19,3,1,0

8,"Slides, materials, and boardwork were understandable."
8,10,24,2,1,0

9,The teacher verified student reactions in class.
9,18,15,4,0,0

10,The teacher worked with enthusiasm.
10,23,14,0,0,0

11,The teacher started and ended class on time.
11,17,19,1,0,0

12,The teacher showed how to prepare and review.
12,19,17,1,0,0
